section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte[rax+rdi], 0
        je .break
        inc rax
        jmp .loop

    .break:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi

    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1

    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rsi, rsp
    pop rdi

    mov rax, 1
    mov rdi, 1

    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 10
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov r9, rsp
    mov r10, 10
    push 0

    .loop:
        mov rdx, 0
        div r10
        add rdx, 0x30
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        jnz .loop
        jmp .execute

    .execute:
        mov rdi, rsp
        push r9
        call print_string
        pop r9
        mov rsp, r9
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    cmp rdi, 0
    jge .cout

    push rdi
    mov rdi, 0x2D
    call print_char
    pop rdi
    neg rdi

    .cout:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8

    .loop:
        mov r10b, byte [rdi + r8]
        cmp byte [rsi + r8], r10b
        jne .zero
        cmp r10b, 0
        je .first
        inc r8
        jmp .loop

    .first:
        mov r9, 1
        jmp .break

    .zero:
        mov r9, 0
        jmp .break

    .break:
        mov rax, r9
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rdx, 1
    xor rdi, rdi
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rcx, rcx

    .loop:
        push rcx
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        pop rcx

        cmp rax, 0
        jz .break

        cmp rax, 0x20
        jz .space

        cmp rax, 0x9
        jz .space

        cmp rax, 0xA
        jz .space

        cmp rcx, rsi
        jge .control

        mov [rdi+rcx], rax
        inc rcx

        jmp .loop

    .control:
            xor rax, rax
            xor rdx, rdx
            ret

    .space:
            cmp rcx, 0
            jz .loop
            jmp .break

    .break:
            mov byte[rdi+rcx], 0
            mov rax, rdi
            mov rdx, rcx
            ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r10, r10
    xor rsi, rsi

    .loop:
        mov rcx, 0xA
        mov sil, [rdi + r10]
        cmp sil, 0x39
        jg .break

        sub sil, 0x30
        jl .break

        inc r10
        mul rcx
        add rax, rsi
        jmp .loop

    .break:
        mov rdx, r10
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    xor rsi, rsi

    cmp byte[rdi], '-'
    jne .negative_number
    mov rsi, 1
    inc rdi

    .negative_number:
        push rsi
        call parse_uint
        pop rsi
        cmp rsi, 0
        je .end
        cmp rdi, 0
        je .end
        inc rdx
        neg rax
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx

    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    jbe .control

    .loop:
        cmp rcx, rdx
        jz .control
        mov r9b, byte[rdi+rcx]
        mov byte[rsi+rcx], r9b
        cmp r9b, 0
        inc rcx
        jz .break
        jmp .loop

    .control:
        xor rax, rax
        ret

    .break:
        mov rax, rcx
        ret
